package model;

import java.util.Date;

public class HeartRates {

	String firstName;
	String lastName;
	Date dateOfBirth;
	
	public HeartRates(String firstName,String lastName,Date dateOfBirth){
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
	}
	
	public HeartRates(String firstName,String lastName){
		this.firstName = firstName;
		this.lastName = lastName;	
	}
	
	public HeartRates(){}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String toString() {
		return "HeartRates [firstName=" + firstName + ", lastName=" + lastName
				+ ", dateOfBirth=" + dateOfBirth + "]";
	}
	
	
}