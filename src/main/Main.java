package main;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.HeartRates;
import services.HeartRatesImpl;

public class Main {

	public static void main(String[] args) throws ParseException {
		HeartRates heartRates = new HeartRates();
		HeartRatesImpl heartRatesImpl = new HeartRatesImpl();
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date dateOfBirth = (Date)formatter.parse("09-05-1992");
		
		int currentAge = HeartRatesImpl.calculateAge(dateOfBirth);
		
		HeartRates HR = new HeartRates();
		
		HeartRates HR2 = new HeartRates("Vasiliki","Gialeli",dateOfBirth );
		
		System.out.println("firstName:" + HR2.getFirstName() + ",lastName: " + HR2.getLastName() 
				+ ",dateOfBirth: " + HR2.getDateOfBirth() + ",Age: " + currentAge + "Years" 
				+ ",maxHeartRate: " + HeartRatesImpl.maxHeartRate(currentAge) 
				+ ",targetHeartRate: " + HeartRatesImpl.targetHeartRate(HeartRatesImpl.maxHeartRate(currentAge)));
	}


}
