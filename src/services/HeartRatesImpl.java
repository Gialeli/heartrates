package services;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author ΒΑΣΙΛΙΚΗ
 *
 */
public class HeartRatesImpl {

	/**
	 * calculate Age
	 * @param dateOfBirth
	 * @return
	 */
	public static int calculateAge(Date dateOfBirth) {
		Calendar BirthDate = Calendar.getInstance();
		BirthDate.setTime(dateOfBirth); // Date of Birth

		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(new Date()); // Now Date

		return currentDate.get(Calendar.YEAR) - BirthDate.get(Calendar.YEAR);

	}

	/**
	 * find max HeartRate
	 * @param years
	 * @return
	 */
	public static int maxHeartRate(int years) {
		int m = 220 - years;
		return m;
	}

	/**
	 * find target HeartRate
	 * @param maximumHeartRate
	 * @return
	 */
	public static int targetHeartRate(int maximumHeartRate) {
		int n = 50 - (85 * maximumHeartRate) / 100;
		return n;
	}

}
